// Copyright © 2023 TXA PTE. LTD.

package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"sdp/cache"
	"sdp/config"
	"sdp/constants"
	"sdp/db"
	"sdp/handler"
	"sdp/indexer"
	"sdp/logger"
	"sdp/queue"
	"sdp/server"
	"sdp/store"
	"sdp/worker"
	"sdp/wsconn"

	"gorm.io/gorm"
)

func main() {
	log := logger.Default
	idle := make(chan struct{})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	s := logger.NewSentry()
	defer s.Flush(2 * time.Second)

	// Data Access / Queue + Cache
	queue := getOrDie(queue.NewQueue(config.GetRedisURL(), config.GetRedisPassword())).(*queue.Queue)
	db := getOrDie(db.NewDb(config.GetPgURL())).(*gorm.DB)

	// Message sources
	controller := wsconn.NewController(queue)
	indexer := indexer.NewIndexer(queue, time.Second)

	// Health server
	server := server.NewServer(db, queue, controller)

	// Workers
	tradeWorker := worker.NewDbWorker(constants.TRADE_WORKER, store.NewTradeStore(db), cache.NewCache(queue))
	orderWorker := worker.NewDbWorker(constants.ORDER_WORKER, store.NewOrderStore(db), cache.NewCache(queue))
	eventWorker := worker.NewDbWorker(constants.EVENT_WORKER, store.NewEventStore(db), cache.NewCache(queue))

	// Processor
	handler := handler.NewHandler(db, queue, controller)
	go handler.Start(ctx)

	go func() {
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		<-quit

		if err := indexer.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down grpc server: %v", err)
		}
		if err := controller.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down websocket server: %v", err)
		}
		if err := server.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down health server: %v", err)
		}
		if err := tradeWorker.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down trade worker: %v", err)
		}
		if err := orderWorker.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down order worker: %v", err)
		}
		if err := eventWorker.Close(); err != nil {
			logger.CaptureException(err)
			log.Errorf("error shutting down event worker: %v", err)
		}
		close(idle)
	}()

	go eventWorker.Start(ctx)
	go handler.ProcessEvents(ctx)

	go orderWorker.Start(ctx)
	go handler.ProcessOrders(ctx)

	go tradeWorker.Start(ctx)
	go handler.ProcessTrades(ctx)

	go server.Start(ctx)

	go controller.Start(ctx)
	go indexer.Start(ctx)

	log.Info("SDP is Ready")

	<-idle
}

func getOrDie(thing any, err error) any {
	if err != nil {
		logger.CaptureException(err)
		log.Fatal(err)
	}
	return thing
}
