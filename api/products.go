// Copyright © 2023 TXA PTE. LTD.

package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"sdp/config"
	"sdp/constants"
	"sdp/logger"
	"sdp/retry"
	"sdp/types"
)

var (
	products        = make(types.Products)
	getProductsOnce sync.Once
	log             = logger.Default
)

func fetchProducts() *types.ApiProducts {
	var res *http.Response
	var pr *types.ApiProducts
	var err error

	action := func(_ uint) error {
		if res, err = http.Get(config.GetInternalApi() + "/products"); err != nil {
			log.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		log.Error(err)
		panic(err)
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal(body, &pr); err != nil {
		panic(err)
	}

	return pr
}

func fetchAsset(id string) *types.ApiAsset {
	var a *types.ApiAsset
	var res *http.Response
	var err error

	action := func(_ uint) error {
		if res, err = http.Get(config.GetInternalApi() + "/assets/" + id); err != nil {
			log.Error(err)
			return err
		}
		return nil
	}
	if err := retry.Do(action); err != nil {
		log.Error(err)
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	if err = json.Unmarshal(body, &a); err != nil {
		panic(err)
	}

	lc := strings.ToLower(a.ContractAddress)
	a.ContractAddress = lc

	return a
}

// GetProducts will query the Internal API for product IDs and iterate the returned products to fetch individual assets.
// These assets are supported by the SDP and need only be fetched once at startup.
func GetProducts() types.Products {
	getProductsOnce.Do(func() {
		if config.GetTestFlag() {
			log.Info("Using TEST products")
			products[constants.ETHUSDT] = &types.Product{
				BaseAsset: types.ApiAsset{
					ContractAddress: constants.ZeroAddress,
					EvmChainId:      5,
					Precision:       10,
					NativePrecision: 18,
				},
				CounterAsset: types.ApiAsset{
					ContractAddress: constants.UsdtGoerliAddress,
					EvmChainId:      5,
					Precision:       10,
					NativePrecision: 18,
				},
			}
			products[constants.ETHDMC] = &types.Product{
				BaseAsset: types.ApiAsset{
					ContractAddress: constants.ZeroAddress,
					EvmChainId:      17001,
					Precision:       6,
					NativePrecision: 18,
				},
				CounterAsset: types.ApiAsset{
					ContractAddress: constants.DmcAddress,
					EvmChainId:      17001,
					Precision:       6,
					NativePrecision: 6,
				},
			}
		} else {
			log.Info("Using Internal API products")
			ps := fetchProducts()
			for _, p := range *ps {
				var aggLevels []uint64

				b := fetchAsset(p.BaseAssetId)
				c := fetchAsset(p.CounterAssetId)

				for _, a := range p.AggLevels {
					n, err := strconv.ParseUint(a, 10, 64)
					if err != nil {
						panic(err)
					}
					aggLevels = append(aggLevels, n)
				}
				products[p.Id] = types.NewProduct(*b, *c, aggLevels)
			}
		}
	})
	return products
}
