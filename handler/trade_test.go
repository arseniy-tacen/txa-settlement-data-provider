// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"testing"

	"sdp/model"
)

func TestProcessTrade(t *testing.T) {
	h, cleanup := setup(t)
	defer cleanup(t)

	validTrade := &model.Trade{
		SdpSequenceNumber: 1,
		SellerBaseEscrow: model.EscrowInfo{
			BeneficiaryAddress: "0x10",
		},
		SellerCounterEscrow: model.EscrowInfo{
			BeneficiaryAddress: "0x10",
		},
		BuyerBaseEscrow: model.EscrowInfo{
			BeneficiaryAddress: "0x9",
		},
		BuyerCounterEscrow: model.EscrowInfo{
			BeneficiaryAddress: "0x9",
		},
		Size:    "5000000000000000000",
		Price:   "100000000",
		Product: "ETH/DMC",
	}

	for _, testCase := range []struct {
		label      string
		trade      *model.Trade
		shouldFail bool
	}{
		{
			label:      "should process valid trade where obligations exist",
			trade:      validTrade,
			shouldFail: false,
		},
	} {
		t.Run(testCase.label, func(t *testing.T) {
			err := h.processTrade(testCase.trade)
			if (err != nil) != testCase.shouldFail {
				t.Errorf("%s shouldFail %t, but got error: %v", testCase.label, testCase.shouldFail, err)
				t.FailNow()
			}
		})
	}
}
