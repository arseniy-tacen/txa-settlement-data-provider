// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"sdp/model"
	"sdp/store"

	"github.com/go-redis/redis/v8"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

func (h *Handler) ProcessOrders(ctx context.Context) {
	os := store.NewOrderStore(h.db)
	latestReceivedOrder := os.MostRecentOrder()

	duration := tickerDuration
	ticker := time.NewTicker(duration)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			foundOrders, err := h.processMissedOrders(ctx, os, latestReceivedOrder)
			if err != nil {
				h.logException(err)
				return
			}
			if err := h.broadcastOrderRequest(os); err != nil {
				h.logException(err)
				return
			}
			if !foundOrders && duration <= maxTickerDuration {
				duration = duration * 2
				ticker.Reset(duration)
			}
		case lis := <-h.orderChan:
			if err := h.processNewOrder(ctx, lis, os, &latestReceivedOrder); err != nil {
				h.logException(err)
				return
			}
			duration = tickerDuration
			ticker.Reset(duration)
		}
	}
}

func (h *Handler) processNewOrder(ctx context.Context, lis *pq.Notification, os *store.OrderStore, largestReceivedOrder *uint64) error {
	sequenceNumber, err := strconv.ParseUint(lis.Extra, 10, 64)
	if err != nil {
		return err
	}
	lastTrade, err := os.GetLastProcessedTrade()
	if err != nil {
		return err
	}
	if sequenceNumber > *largestReceivedOrder {
		*largestReceivedOrder = sequenceNumber
	}
	order, err := h.getNextOrder(lastTrade.SdpSequenceNumber+1, os)
	if err != nil {
		return err
	}
	return h.processOrder(order)
}

func (h *Handler) broadcastOrderRequest(os *store.OrderStore) error {
	lastTrade, err := os.GetLastProcessedTrade()
	if err != nil {
		return err
	}
	seq := lastTrade.SdpSequenceNumber + 1
	h.controller.Broadcast(seq)
	h.log.Infof("sending broadcast for order sequence: %d", seq)
	return nil
}

func (h *Handler) processMissedOrders(ctx context.Context, os *store.OrderStore, largestReceivedOrder uint64) (bool, error) {
	lastTrade, err := os.GetLastProcessedTrade()
	if err != nil {
		return false, err
	}
	foundMissingOrder := false
	for nextSequence := lastTrade.SdpSequenceNumber + 1; nextSequence <= largestReceivedOrder; nextSequence++ {
		order, err := h.getNextOrder(nextSequence, os)
		if err != nil {
			return true, err
		}
		if err := h.processOrder(order); err != nil {
			return true, err
		}
		foundMissingOrder = true
	}
	return foundMissingOrder, nil
}

func (h *Handler) getNextOrder(nextOrderId uint64, store *store.OrderStore) (*model.OrdersCanceledForSettlement, error) {
	order, err := store.GetBySeq(nextOrderId)
	if err == nil && order != nil {
		return order, nil
	}
	s, err := h.queue.Get(context.Background(), fmt.Sprintf("order_%d", nextOrderId))
	if err == nil {
		order, err = order.UnmarshalBinary([]byte(s))
		if err != nil {
			return nil, err
		}
		return nil, store.Create(order)
	}
	if err != redis.Nil {
		return nil, err
	}
	return nil, nil
}

func (h *Handler) processOrder(order *model.OrdersCanceledForSettlement) error {
	var event *model.Event
	var err error

	if order == nil {
		return nil
	}
	if order.Processed {
		return nil
	}

	es := store.NewEventStore(h.db)
	ts := store.NewTradeStore(h.db)

	lpt, err := ts.GetLastProcessedTrade()
	if err != nil {
		return err
	}

	if order.SdpSequenceNumber != lpt.SdpSequenceNumber+1 {
		return nil
	}

	var tries uint = 0
	var retryAttempts uint = 15

	for tries <= retryAttempts {
		event, err = es.GetBySettlementId(
			order.SettlementId,
			uint64(order.ChainId),
			strings.ToLower(order.Address),
		)
		if err != nil {
			return fmt.Errorf("error requesting settlement event: %v", err)
		}
		if event != nil {
			break
		}
		h.log.Warnf("couldn't find settlement_request, retrying")
		time.Sleep(time.Second * 2)
	}

	return h.db.Transaction(func(tx *gorm.DB) error {
		es = store.NewEventStore(tx)
		ts = store.NewTradeStore(tx)
		os := store.NewOrderStore(tx)

		// If the event is completed, nothing needs to be done
		if event.Completed {
			return nil
		}

		// Prepare obligations for report
		obligations, err := h.FinalizeObligations(
			strings.ToLower(event.Wallet),
			strings.ToLower(event.Token),
			event.SettlementId,
			event.ChainId,
		)
		if err != nil {
			h.log.Fatalf("failed to finalize obligations")
		}

		reporter, ok := h.reporters[fmt.Sprintf("%d", event.ChainId)]
		if !ok {
			return fmt.Errorf("tried to report to chainId that was not configured as a reporter")
		}
		if err := reporter.ReportObligations(event, obligations); err != nil {
			fallbackReporter, ok := h.reporters[fmt.Sprintf("%d_fallback", event.ChainId)]
			if !ok {
				return fmt.Errorf("failed to report obligations and no fallback provider was configured: %v", err)
			}
			if err := fallbackReporter.ReportObligations(event, obligations); err != nil {
				return fmt.Errorf("tried to report to obligations and died: %v", err)
			}
		}
		if err := es.CompleteEvent(event); err != nil {
			return fmt.Errorf("error updating settlement event in postgres: %v", err)
		}
		h.Decollateralize(event)
		if err := os.ProcessOrder(order); err != nil {
			return fmt.Errorf("failed to mark order processed: %v", err)
		}
		if err := ts.UpdateLastProcessedTrade(order.SdpSequenceNumber); err != nil {
			return fmt.Errorf("failed to update last processed trade: %v", err)
		}
		return nil
	})
}
