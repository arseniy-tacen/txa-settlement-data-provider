// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"sdp/model"
	"sdp/proto_def"
	"sdp/store"

	"github.com/ethereum/go-ethereum/common/math"
	"github.com/go-redis/redis/v8"
	"github.com/lib/pq"
	"gorm.io/gorm"
)

func (h *Handler) ProcessEvents(ctx context.Context) {
	es := store.NewEventStore(h.db)

	duration := tickerDuration

	ticker := time.NewTicker(duration)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			foundMissing, err := h.processMissingEvents(ctx, es)
			if err != nil {
				h.logException(err)
				return
			}
			if !foundMissing && duration <= maxTickerDuration {
				duration = duration * 2
				ticker.Reset(duration)
			}
		case lis := <-h.eventChan:
			if err := h.processNewEvent(lis, es); err != nil {
				h.logException(err)
				return
			}
			duration = tickerDuration
			ticker.Reset(duration)
		}
	}
}

func (h *Handler) processNewEvent(lis *pq.Notification, es *store.EventStore) error {
	event, err := es.GetByTxHash(lis.Extra)
	if err != nil {
		return err
	}
	return h.processEvent(event)
}

func (h *Handler) processMissingEvents(ctx context.Context, es *store.EventStore) (bool, error) {
	events, err := es.GetUnprocessedEvents()
	if err != nil {
		return false, err
	}
	if len(*events) == 0 {
		return false, nil
	}

	foundMissing := false
	for _, event := range *events {
		if err := h.processEvent(&event); err != nil {
			return true, err
		}
		foundMissing = true
	}

	return foundMissing, nil
}

func (h *Handler) getNextEvent(txHash string, store *store.EventStore) (*model.Event, error) {
	event, err := store.GetByTxHash(txHash)
	if err == nil && event != nil {
		return event, nil
	}

	s, err := h.queue.Get(context.Background(), fmt.Sprintf("event_%s", txHash))
	if err == nil {
		event, err = event.UnmarshalBinary([]byte(s))
		if err != nil {
			return nil, err
		}
		return nil, store.Create(event)
	}

	if err != redis.Nil {
		return nil, err
	}

	return nil, nil
}

func (h *Handler) processEvent(e *model.Event) error {
	if e.Processed {
		return nil
	}
	if e.Completed {
		return nil
	}

	h.log.Infof("detected event from indexer: %v", e.GetEventString())
	switch e.GetEventString() {
	case proto_def.EventType_DEPOSIT.String():
		if err := h.Deposit(e); err != nil {
			return err
		}
	case proto_def.EventType_TRADEABLE_TOKEN.String():
		if err := h.TradeableToken(e); err != nil {
			return err
		}
	case proto_def.EventType_SETTLEMENT_REQUEST.String():
		if err := h.SettlementRequest(e); err != nil {
			return err
		}
	case proto_def.EventType_OBLIGATIONS_REPORTED.String():
		if err := h.ObligationsReported(e); err != nil {
			return err
		}
	case proto_def.EventType_OBLIGATIONS_WRITTEN.String():
		if err := h.ObligationsWritten(e); err != nil {
			return err
		}
	case proto_def.EventType_OBLIGATION_WRITTEN.String():
		if err := h.ObligationWritten(e); err != nil {
			return err
		}
	case proto_def.EventType_COLLATERALIZED.String():
		if err := h.Collateralize(e); err != nil {
			return err
		}
	default:
		h.log.Warnf("cannot handle event type %s", e.GetEventString())
		if err := h.CannotHandle(e); err != nil {
			return err
		}
	}
	return nil
}

func (h *Handler) CannotHandle(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) SettlementRequest(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) ObligationsReported(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) ObligationsWritten(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) ObligationWritten(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) TradeableToken(event *model.Event) error {
	es := store.NewEventStore(h.db)
	return es.ProcessEvent(event)
}

func (h *Handler) Deposit(event *model.Event) error {
	return h.db.Transaction(func(tx *gorm.DB) error {
		es := store.NewEventStore(tx)
		if err := h.applyCredit(tx,
			strings.ToLower(event.Wallet),
			strings.ToLower(event.Wallet),
			strings.ToLower(event.Token),
			math.MustParseBig256(event.Amount),
			false,
			event.ChainId,
		); err != nil {
			return err
		}
		if err := es.ProcessEvent(event); err != nil {
			return err
		}
		return nil
	})
}

func (h *Handler) Collateralize(event *model.Event) error {
	return h.db.Transaction(func(tx *gorm.DB) error {
		es := store.NewEventStore(tx)
		amount := math.MustParseBig256(event.Amount)
		if amount.Cmp(big.NewInt(0)) > 0 {
			if err := h.applyCredit(tx,
				strings.ToLower(event.Wallet),
				strings.ToLower(event.Wallet),
				strings.ToLower(event.Token),
				amount,
				false,
				event.ChainId,
			); err != nil {
				return err
			}
		}
		return es.ProcessEvent(event)
	})
}

func (h *Handler) Decollateralize(event *model.Event) error {
	debitFullAmount := func(existingAmount *big.Int) (updatedAmount *big.Int, err error) {
		return big.NewInt(0), nil
	}
	return h.db.Transaction(func(tx *gorm.DB) error {
		return h.updateObligation(
			tx,
			strings.ToLower(event.Wallet),
			strings.ToLower(event.Wallet),
			strings.ToLower(event.Token),
			false,
			event.ChainId,
			debitFullAmount,
		)
	})
}
