// Copyright © 2023 TXA PTE. LTD.

package handler

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"testing"

	"sdp/config"
	"sdp/constants"
	"sdp/db"
	"sdp/model"

	"github.com/gorilla/websocket"
	"gorm.io/gorm"
)

var (
	upgrader = websocket.Upgrader{}
	alice    = strings.ToLower("0xEE214aC2A18bCDf7F563f63a2Cbc30DdAD9DB2E4")
	bob      = strings.ToLower("0x537B304C9B632FFE222C222CD455d2bC6ae6af1f")
)

type stub struct{}

func (s *stub) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Printf("Received message: %s", message)

		err = conn.WriteMessage(websocket.TextMessage, []byte("Hello, client!"))
		if err != nil {
			fmt.Println(err)
			break
		}
	}
}

func setup(t *testing.T) (*Handler, func(t *testing.T)) {
	db, err := db.NewTestDb(config.GetPgURL() + "?sslmode=disable")
	if err != nil {
		panic(err)
	}

	sm := http.NewServeMux()
	h := &stub{}
	sm.Handle("/", h)
	s := &http.Server{
		Handler: sm,
		Addr:    ":8052",
	}

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		s.ListenAndServe()
	}()

	seed(db)

	return NewHandler(db, nil, nil), func(t *testing.T) {
		err := drop(db)
		s.Shutdown(context.Background())
		wg.Wait()
		if err != nil {
			t.Fatal(fmt.Errorf("failed to clean up database: %v", err))
		}
	}
}

func seed(db *gorm.DB) {
	seedObligationTable(db)
	seedEventTable(db)

	db.Save(&model.LastProcessedTrade{SdpSequenceNumber: 0})
}

func seedEventTable(db *gorm.DB) {
	events := []*model.Event{
		{
			BlockNumber:  121,
			EventType:    2,
			Token:        constants.ZeroAddress,
			Wallet:       alice,
			SettlementId: "2",
			ChainId:      17001,
			TxHash:       "0x84ac2889f5add6257d1acd651943025734b0d210e3169392674148d9e5185756",
			Processed:    true,
		},
	}
	db.Create(events)
}

func seedObligationTable(db *gorm.DB) {
	obligations := []*model.Obligation{
		{
			Amount:    "200",
			Source:    "1",
			Sink:      "2",
			Token:     "0x00",
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(1),
		},
		{
			Amount:    "500",
			Sink:      "55",
			Source:    "55",
			Token:     "0x00",
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(1),
		},
		{
			Amount:    "500",
			Sink:      "56",
			Source:    "56",
			Token:     "0x00",
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(1),
		},
		{
			Amount:    "200",
			Source:    "1",
			Sink:      "99",
			Token:     "0x00",
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(1),
		},
		{
			Amount:  "100000000000000000000",
			Source:  alice,
			Sink:    alice,
			Token:   constants.ZeroAddress,
			ChainId: uint64(25),
		},
		{
			Amount:    "50000000000000000000",
			Sink:      "0x9",
			Source:    "0x9",
			Token:     constants.ZeroAddress,
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(17001),
		},
		{
			Amount:    "50000000000000000000",
			Sink:      "0x9",
			Source:    "0x9",
			Token:     constants.DmcAddress,
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(17001),
		},
		{
			Amount:    "50000000000000000000",
			Sink:      "0x10",
			Source:    "0x10",
			Token:     constants.ZeroAddress,
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(17001),
		},
		{
			Amount:    "50000000000000000000",
			Sink:      "0x10",
			Source:    "0x10",
			Token:     constants.DmcAddress,
			Allocated: false,
			Status:    model.STAGED,
			ChainId:   uint64(17001),
		},
		{
			Source:  bob,
			Sink:    alice,
			Token:   constants.ZeroAddress,
			Amount:  "100000000000000000000",
			ChainId: uint64(25),
		},
		{
			Amount:  "150000000000000000",
			Sink:    bob,
			Source:  alice,
			Token:   constants.ZeroAddress,
			ChainId: uint64(17001),
		},
	}

	for _, obligation := range obligations {
		db.Create(obligation)
	}
}

func drop(db *gorm.DB) error {
	s, err := db.DB()
	if err != nil {
		return err
	}
	return s.Close()
}
