// Copyright © 2023 TXA PTE. LTD.

package server

import (
	"context"
	"fmt"
	"net/http"
	_ "net/http/pprof"

	"sdp/config"
	"sdp/logger"
	"sdp/queue"
	"sdp/wsconn"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type server struct {
	srv     *http.Server
	closing chan chan error
	log     *logrus.Logger
}

func NewServer(db *gorm.DB, queue *queue.Queue, controller *wsconn.Controller) *server {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)

	s := &server{
		srv: &http.Server{
			Addr:    config.GetHttpAddress(),
			Handler: r,
		},
		closing: make(chan chan error),
		log:     logger.Default,
	}

	hc := s.registerHealthCheck(db, queue, controller)

	r.Get("/readyz", hc)
	r.Get("/livez", hc)
	r.Mount("/debug", middleware.Profiler())

	return s
}

func (s *server) Start(ctx context.Context) {
	var err error
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case errc := <-s.closing:
				if err = s.srv.Shutdown(context.Background()); err != http.ErrServerClosed {
					errc <- err
				}
				return
			}
		}
	}()

	err = s.srv.ListenAndServe()
}

func (s *server) Close() error {
	errc := make(chan error)
	s.closing <- errc
	return <-errc
}

func (s *server) registerHealthCheck(db *gorm.DB, q *queue.Queue, c *wsconn.Controller) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		if ok := s.checkHealthDb(db); !ok {
			s.log.Error(fmt.Errorf("health checks failed: db not healthy"))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		if ok := s.checkHealthQueue(q); !ok {
			s.log.Error(fmt.Errorf("health checks failed: queue not healthy"))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		w.Write([]byte(http.StatusText(http.StatusOK)))
	}
}

func (s *server) checkHealthQueue(q *queue.Queue) bool {
	if err := q.Ping(); err != nil {
		return false
	}
	return true
}

func (s *server) checkHealthDb(db *gorm.DB) bool {
	sql, err := db.DB()
	if err != nil {
		s.log.Errorf("failed database health check: %v", err)
		return false
	}
	if err := sql.Ping(); err != nil {
		s.log.Errorf("failed database health check: %v", err)
		return false
	}
	return true
}
