#!/bin/sh
asdf=$(kubectl logs -n exchange deployments/exchange-sdp)

while ! echo ${asdf} | grep "DEPOSIT" >/dev/null; do
	sleep 5
	echo "Waiting for SDO to detect deposit event..."
	echo ${asdf}
	asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
done
