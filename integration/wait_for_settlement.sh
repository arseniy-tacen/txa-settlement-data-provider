#!/bin/sh
asdf=$(kubectl logs -n exchange deployments/exchange-sdp)

while ! echo ${asdf} | grep "SETTLEMENT_REQUEST" >/dev/null; do
  sleep 5
  echo "Waiting for SDO to detect settlement message..."
  echo ${asdf}
  asdf=$(kubectl logs -n exchange deployments/exchange-sdp)
done
