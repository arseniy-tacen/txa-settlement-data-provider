// Copyright © 2023 TXA PTE. LTD.

package model

import (
	"encoding/json"

	"sdp/proto_def"

	"gorm.io/gorm"
)

var eventMap = map[int32]string{
	0:  "NEW_WALLET",
	1:  "DEPOSIT",
	2:  "SETTLEMENT_REQUEST",
	3:  "OBLIGATIONS_REPORTED",
	4:  "OBLIGATIONS_WRITTEN",
	5:  "COLLATERALIZED",
	6:  "WITHDRAW",
	7:  "TRADEABLE_TOKEN",
	8:  "REQUEST_FUNDS_OWED",
	9:  "KEY_APPROVED",
	10: "OBLIGATION_WRITTEN",
}

type Event struct {
	gorm.Model
	BlockNumber  uint64 `gorm:"type:numeric"`
	EventType    int32
	From         string
	To           string
	Amount       string
	Token        string
	Wallet       string
	Coordinator  string
	SettlementId string
	ChainId      uint64
	TxHash       string
	Completed    bool
	Processed    bool `gorm:"default:false"`
}

func (e *Event) MarshalBinary() ([]byte, error) {
	return json.Marshal(e)
}

func (e *Event) UnmarshalBinary(data []byte) (*Event, error) {
	if err := json.Unmarshal(data, &e); err != nil {
		return nil, err
	}
	return e, nil
}

func (e *Event) GetEventString() string {
	return eventMap[e.EventType]
}

func (e *Event) Decode(pem *proto_def.ParsedEventMessage) *Event {
	*e = Event{
		BlockNumber:  pem.GetBlockNumber(),
		EventType:    int32(pem.GetEventType()),
		From:         pem.GetFrom(),
		To:           pem.GetTo(),
		Amount:       pem.GetAmount(),
		Token:        pem.GetToken(),
		Wallet:       pem.GetWallet(),
		Coordinator:  pem.GetCoordinator(),
		SettlementId: pem.GetSettlementId(),
		ChainId:      pem.GetChainId(),
		TxHash:       pem.GetTxHash(),
	}
	return e
}
