// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package abis

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// IdentityRegistryMetaData contains all meta data concerning the IdentityRegistry contract.
var IdentityRegistryMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"governance\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_unlockInterval\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"publicKey\",\"type\":\"bytes\"}],\"name\":\"KeyApproved\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"trader\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"publicKey\",\"type\":\"bytes\"}],\"name\":\"KeyRevoked\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"previousAdminRole\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"newAdminRole\",\"type\":\"bytes32\"}],\"name\":\"RoleAdminChanged\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"}],\"name\":\"RoleGranted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"}],\"name\":\"RoleRevoked\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"DEFAULT_ADMIN_ROLE\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"EXPIRATION_TIME\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"},{\"internalType\":\"bytes\",\"name\":\"publicKey\",\"type\":\"bytes\"}],\"name\":\"approveSignature\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"auditorReward\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"n\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"d\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"calculateAuditorReward\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"calculateCollateral\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"calculateFee\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"collateralPercent\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"n\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"d\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"fee\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"n\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"d\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"feeRecipient\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"version\",\"type\":\"uint256\"}],\"name\":\"getCoordinator\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getGovernanceAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestAssetCustody\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestCollateralCustody\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestConsensus\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestCoordinator\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestCoordinatorVersion\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getLatestProtocolToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"}],\"name\":\"getRoleAdmin\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getRoleMember\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"}],\"name\":\"getRoleMemberCount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"grantRole\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"admin\",\"type\":\"address\"}],\"name\":\"grantSDPAdmin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"hasRole\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"initialAddress\",\"type\":\"address\"}],\"name\":\"initializeRole\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"auditor\",\"type\":\"address\"}],\"name\":\"isAuditor\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"}],\"name\":\"isProtocolToken\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"admin\",\"type\":\"address\"}],\"name\":\"isSDPAdmin\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"token\",\"type\":\"address\"}],\"name\":\"isTradeableToken\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"coord\",\"type\":\"address\"}],\"name\":\"isUnexpiredCoordinator\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"coord\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"versionId\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minVersionId\",\"type\":\"uint256\"}],\"name\":\"isUnexpiredCoordinator\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"protocolStakeAmount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"renounceRole\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"revokeRole\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"revokeSignature\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_feeRecipient\",\"type\":\"address\"}],\"name\":\"setFeeRecipient\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_protocolStakeAmount\",\"type\":\"uint256\"}],\"name\":\"setStakeAmount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"signatures\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"v\",\"type\":\"uint8\"},{\"internalType\":\"bytes32\",\"name\":\"r\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"s\",\"type\":\"bytes32\"},{\"internalType\":\"bytes\",\"name\":\"publicKey\",\"type\":\"bytes\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unlockInterval\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"role\",\"type\":\"bytes32\"},{\"internalType\":\"address\",\"name\":\"newAddress\",\"type\":\"address\"}],\"name\":\"updateRole\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"versionInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"expirationDate\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// IdentityRegistryABI is the input ABI used to generate the binding from.
// Deprecated: Use IdentityRegistryMetaData.ABI instead.
var IdentityRegistryABI = IdentityRegistryMetaData.ABI

// IdentityRegistry is an auto generated Go binding around an Ethereum contract.
type IdentityRegistry struct {
	IdentityRegistryCaller     // Read-only binding to the contract
	IdentityRegistryTransactor // Write-only binding to the contract
	IdentityRegistryFilterer   // Log filterer for contract events
}

// IdentityRegistryCaller is an auto generated read-only Go binding around an Ethereum contract.
type IdentityRegistryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IdentityRegistryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IdentityRegistryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IdentityRegistryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IdentityRegistryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IdentityRegistrySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IdentityRegistrySession struct {
	Contract     *IdentityRegistry // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IdentityRegistryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IdentityRegistryCallerSession struct {
	Contract *IdentityRegistryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts           // Call options to use throughout this session
}

// IdentityRegistryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IdentityRegistryTransactorSession struct {
	Contract     *IdentityRegistryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts           // Transaction auth options to use throughout this session
}

// IdentityRegistryRaw is an auto generated low-level Go binding around an Ethereum contract.
type IdentityRegistryRaw struct {
	Contract *IdentityRegistry // Generic contract binding to access the raw methods on
}

// IdentityRegistryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IdentityRegistryCallerRaw struct {
	Contract *IdentityRegistryCaller // Generic read-only contract binding to access the raw methods on
}

// IdentityRegistryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IdentityRegistryTransactorRaw struct {
	Contract *IdentityRegistryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIdentityRegistry creates a new instance of IdentityRegistry, bound to a specific deployed contract.
func NewIdentityRegistry(address common.Address, backend bind.ContractBackend) (*IdentityRegistry, error) {
	contract, err := bindIdentityRegistry(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistry{IdentityRegistryCaller: IdentityRegistryCaller{contract: contract}, IdentityRegistryTransactor: IdentityRegistryTransactor{contract: contract}, IdentityRegistryFilterer: IdentityRegistryFilterer{contract: contract}}, nil
}

// NewIdentityRegistryCaller creates a new read-only instance of IdentityRegistry, bound to a specific deployed contract.
func NewIdentityRegistryCaller(address common.Address, caller bind.ContractCaller) (*IdentityRegistryCaller, error) {
	contract, err := bindIdentityRegistry(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryCaller{contract: contract}, nil
}

// NewIdentityRegistryTransactor creates a new write-only instance of IdentityRegistry, bound to a specific deployed contract.
func NewIdentityRegistryTransactor(address common.Address, transactor bind.ContractTransactor) (*IdentityRegistryTransactor, error) {
	contract, err := bindIdentityRegistry(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryTransactor{contract: contract}, nil
}

// NewIdentityRegistryFilterer creates a new log filterer instance of IdentityRegistry, bound to a specific deployed contract.
func NewIdentityRegistryFilterer(address common.Address, filterer bind.ContractFilterer) (*IdentityRegistryFilterer, error) {
	contract, err := bindIdentityRegistry(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryFilterer{contract: contract}, nil
}

// bindIdentityRegistry binds a generic wrapper to an already deployed contract.
func bindIdentityRegistry(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IdentityRegistryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IdentityRegistry *IdentityRegistryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IdentityRegistry.Contract.IdentityRegistryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IdentityRegistry *IdentityRegistryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.IdentityRegistryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IdentityRegistry *IdentityRegistryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.IdentityRegistryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IdentityRegistry *IdentityRegistryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IdentityRegistry.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IdentityRegistry *IdentityRegistryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IdentityRegistry *IdentityRegistryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.contract.Transact(opts, method, params...)
}

// DEFAULTADMINROLE is a free data retrieval call binding the contract method 0xa217fddf.
//
// Solidity: function DEFAULT_ADMIN_ROLE() view returns(bytes32)
func (_IdentityRegistry *IdentityRegistryCaller) DEFAULTADMINROLE(opts *bind.CallOpts) ([32]byte, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "DEFAULT_ADMIN_ROLE")

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// DEFAULTADMINROLE is a free data retrieval call binding the contract method 0xa217fddf.
//
// Solidity: function DEFAULT_ADMIN_ROLE() view returns(bytes32)
func (_IdentityRegistry *IdentityRegistrySession) DEFAULTADMINROLE() ([32]byte, error) {
	return _IdentityRegistry.Contract.DEFAULTADMINROLE(&_IdentityRegistry.CallOpts)
}

// DEFAULTADMINROLE is a free data retrieval call binding the contract method 0xa217fddf.
//
// Solidity: function DEFAULT_ADMIN_ROLE() view returns(bytes32)
func (_IdentityRegistry *IdentityRegistryCallerSession) DEFAULTADMINROLE() ([32]byte, error) {
	return _IdentityRegistry.Contract.DEFAULTADMINROLE(&_IdentityRegistry.CallOpts)
}

// EXPIRATIONTIME is a free data retrieval call binding the contract method 0x4a5c7348.
//
// Solidity: function EXPIRATION_TIME() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) EXPIRATIONTIME(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "EXPIRATION_TIME")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// EXPIRATIONTIME is a free data retrieval call binding the contract method 0x4a5c7348.
//
// Solidity: function EXPIRATION_TIME() view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) EXPIRATIONTIME() (*big.Int, error) {
	return _IdentityRegistry.Contract.EXPIRATIONTIME(&_IdentityRegistry.CallOpts)
}

// EXPIRATIONTIME is a free data retrieval call binding the contract method 0x4a5c7348.
//
// Solidity: function EXPIRATION_TIME() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) EXPIRATIONTIME() (*big.Int, error) {
	return _IdentityRegistry.Contract.EXPIRATIONTIME(&_IdentityRegistry.CallOpts)
}

// AuditorReward is a free data retrieval call binding the contract method 0x1487e567.
//
// Solidity: function auditorReward() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCaller) AuditorReward(opts *bind.CallOpts) (struct {
	N *big.Int
	D *big.Int
}, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "auditorReward")

	outstruct := new(struct {
		N *big.Int
		D *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.N = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.D = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// AuditorReward is a free data retrieval call binding the contract method 0x1487e567.
//
// Solidity: function auditorReward() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistrySession) AuditorReward() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.AuditorReward(&_IdentityRegistry.CallOpts)
}

// AuditorReward is a free data retrieval call binding the contract method 0x1487e567.
//
// Solidity: function auditorReward() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCallerSession) AuditorReward() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.AuditorReward(&_IdentityRegistry.CallOpts)
}

// CalculateAuditorReward is a free data retrieval call binding the contract method 0xdf6622b2.
//
// Solidity: function calculateAuditorReward(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) CalculateAuditorReward(opts *bind.CallOpts, amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "calculateAuditorReward", amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateAuditorReward is a free data retrieval call binding the contract method 0xdf6622b2.
//
// Solidity: function calculateAuditorReward(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) CalculateAuditorReward(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateAuditorReward(&_IdentityRegistry.CallOpts, amount)
}

// CalculateAuditorReward is a free data retrieval call binding the contract method 0xdf6622b2.
//
// Solidity: function calculateAuditorReward(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) CalculateAuditorReward(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateAuditorReward(&_IdentityRegistry.CallOpts, amount)
}

// CalculateCollateral is a free data retrieval call binding the contract method 0x56059339.
//
// Solidity: function calculateCollateral(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) CalculateCollateral(opts *bind.CallOpts, amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "calculateCollateral", amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateCollateral is a free data retrieval call binding the contract method 0x56059339.
//
// Solidity: function calculateCollateral(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) CalculateCollateral(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateCollateral(&_IdentityRegistry.CallOpts, amount)
}

// CalculateCollateral is a free data retrieval call binding the contract method 0x56059339.
//
// Solidity: function calculateCollateral(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) CalculateCollateral(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateCollateral(&_IdentityRegistry.CallOpts, amount)
}

// CalculateFee is a free data retrieval call binding the contract method 0x99a5d747.
//
// Solidity: function calculateFee(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) CalculateFee(opts *bind.CallOpts, amount *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "calculateFee", amount)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// CalculateFee is a free data retrieval call binding the contract method 0x99a5d747.
//
// Solidity: function calculateFee(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) CalculateFee(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateFee(&_IdentityRegistry.CallOpts, amount)
}

// CalculateFee is a free data retrieval call binding the contract method 0x99a5d747.
//
// Solidity: function calculateFee(uint256 amount) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) CalculateFee(amount *big.Int) (*big.Int, error) {
	return _IdentityRegistry.Contract.CalculateFee(&_IdentityRegistry.CallOpts, amount)
}

// CollateralPercent is a free data retrieval call binding the contract method 0xccc12094.
//
// Solidity: function collateralPercent() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCaller) CollateralPercent(opts *bind.CallOpts) (struct {
	N *big.Int
	D *big.Int
}, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "collateralPercent")

	outstruct := new(struct {
		N *big.Int
		D *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.N = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.D = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// CollateralPercent is a free data retrieval call binding the contract method 0xccc12094.
//
// Solidity: function collateralPercent() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistrySession) CollateralPercent() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.CollateralPercent(&_IdentityRegistry.CallOpts)
}

// CollateralPercent is a free data retrieval call binding the contract method 0xccc12094.
//
// Solidity: function collateralPercent() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCallerSession) CollateralPercent() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.CollateralPercent(&_IdentityRegistry.CallOpts)
}

// Fee is a free data retrieval call binding the contract method 0xddca3f43.
//
// Solidity: function fee() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCaller) Fee(opts *bind.CallOpts) (struct {
	N *big.Int
	D *big.Int
}, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "fee")

	outstruct := new(struct {
		N *big.Int
		D *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.N = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.D = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// Fee is a free data retrieval call binding the contract method 0xddca3f43.
//
// Solidity: function fee() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistrySession) Fee() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.Fee(&_IdentityRegistry.CallOpts)
}

// Fee is a free data retrieval call binding the contract method 0xddca3f43.
//
// Solidity: function fee() view returns(uint256 n, uint256 d)
func (_IdentityRegistry *IdentityRegistryCallerSession) Fee() (struct {
	N *big.Int
	D *big.Int
}, error) {
	return _IdentityRegistry.Contract.Fee(&_IdentityRegistry.CallOpts)
}

// FeeRecipient is a free data retrieval call binding the contract method 0x46904840.
//
// Solidity: function feeRecipient() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) FeeRecipient(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "feeRecipient")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// FeeRecipient is a free data retrieval call binding the contract method 0x46904840.
//
// Solidity: function feeRecipient() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) FeeRecipient() (common.Address, error) {
	return _IdentityRegistry.Contract.FeeRecipient(&_IdentityRegistry.CallOpts)
}

// FeeRecipient is a free data retrieval call binding the contract method 0x46904840.
//
// Solidity: function feeRecipient() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) FeeRecipient() (common.Address, error) {
	return _IdentityRegistry.Contract.FeeRecipient(&_IdentityRegistry.CallOpts)
}

// GetCoordinator is a free data retrieval call binding the contract method 0x42424d6f.
//
// Solidity: function getCoordinator(uint256 version) view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetCoordinator(opts *bind.CallOpts, version *big.Int) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getCoordinator", version)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetCoordinator is a free data retrieval call binding the contract method 0x42424d6f.
//
// Solidity: function getCoordinator(uint256 version) view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetCoordinator(version *big.Int) (common.Address, error) {
	return _IdentityRegistry.Contract.GetCoordinator(&_IdentityRegistry.CallOpts, version)
}

// GetCoordinator is a free data retrieval call binding the contract method 0x42424d6f.
//
// Solidity: function getCoordinator(uint256 version) view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetCoordinator(version *big.Int) (common.Address, error) {
	return _IdentityRegistry.Contract.GetCoordinator(&_IdentityRegistry.CallOpts, version)
}

// GetGovernanceAddress is a free data retrieval call binding the contract method 0x73252494.
//
// Solidity: function getGovernanceAddress() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetGovernanceAddress(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getGovernanceAddress")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetGovernanceAddress is a free data retrieval call binding the contract method 0x73252494.
//
// Solidity: function getGovernanceAddress() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetGovernanceAddress() (common.Address, error) {
	return _IdentityRegistry.Contract.GetGovernanceAddress(&_IdentityRegistry.CallOpts)
}

// GetGovernanceAddress is a free data retrieval call binding the contract method 0x73252494.
//
// Solidity: function getGovernanceAddress() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetGovernanceAddress() (common.Address, error) {
	return _IdentityRegistry.Contract.GetGovernanceAddress(&_IdentityRegistry.CallOpts)
}

// GetLatestAssetCustody is a free data retrieval call binding the contract method 0x22e3c2bd.
//
// Solidity: function getLatestAssetCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestAssetCustody(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestAssetCustody")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLatestAssetCustody is a free data retrieval call binding the contract method 0x22e3c2bd.
//
// Solidity: function getLatestAssetCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestAssetCustody() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestAssetCustody(&_IdentityRegistry.CallOpts)
}

// GetLatestAssetCustody is a free data retrieval call binding the contract method 0x22e3c2bd.
//
// Solidity: function getLatestAssetCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestAssetCustody() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestAssetCustody(&_IdentityRegistry.CallOpts)
}

// GetLatestCollateralCustody is a free data retrieval call binding the contract method 0xd55df48d.
//
// Solidity: function getLatestCollateralCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestCollateralCustody(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestCollateralCustody")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLatestCollateralCustody is a free data retrieval call binding the contract method 0xd55df48d.
//
// Solidity: function getLatestCollateralCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestCollateralCustody() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestCollateralCustody(&_IdentityRegistry.CallOpts)
}

// GetLatestCollateralCustody is a free data retrieval call binding the contract method 0xd55df48d.
//
// Solidity: function getLatestCollateralCustody() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestCollateralCustody() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestCollateralCustody(&_IdentityRegistry.CallOpts)
}

// GetLatestConsensus is a free data retrieval call binding the contract method 0xd1ce87bc.
//
// Solidity: function getLatestConsensus() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestConsensus(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestConsensus")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLatestConsensus is a free data retrieval call binding the contract method 0xd1ce87bc.
//
// Solidity: function getLatestConsensus() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestConsensus() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestConsensus(&_IdentityRegistry.CallOpts)
}

// GetLatestConsensus is a free data retrieval call binding the contract method 0xd1ce87bc.
//
// Solidity: function getLatestConsensus() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestConsensus() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestConsensus(&_IdentityRegistry.CallOpts)
}

// GetLatestCoordinator is a free data retrieval call binding the contract method 0x4293323a.
//
// Solidity: function getLatestCoordinator() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestCoordinator(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestCoordinator")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLatestCoordinator is a free data retrieval call binding the contract method 0x4293323a.
//
// Solidity: function getLatestCoordinator() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestCoordinator() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestCoordinator(&_IdentityRegistry.CallOpts)
}

// GetLatestCoordinator is a free data retrieval call binding the contract method 0x4293323a.
//
// Solidity: function getLatestCoordinator() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestCoordinator() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestCoordinator(&_IdentityRegistry.CallOpts)
}

// GetLatestCoordinatorVersion is a free data retrieval call binding the contract method 0xd208e994.
//
// Solidity: function getLatestCoordinatorVersion() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestCoordinatorVersion(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestCoordinatorVersion")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetLatestCoordinatorVersion is a free data retrieval call binding the contract method 0xd208e994.
//
// Solidity: function getLatestCoordinatorVersion() view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestCoordinatorVersion() (*big.Int, error) {
	return _IdentityRegistry.Contract.GetLatestCoordinatorVersion(&_IdentityRegistry.CallOpts)
}

// GetLatestCoordinatorVersion is a free data retrieval call binding the contract method 0xd208e994.
//
// Solidity: function getLatestCoordinatorVersion() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestCoordinatorVersion() (*big.Int, error) {
	return _IdentityRegistry.Contract.GetLatestCoordinatorVersion(&_IdentityRegistry.CallOpts)
}

// GetLatestProtocolToken is a free data retrieval call binding the contract method 0x5b1fcd6d.
//
// Solidity: function getLatestProtocolToken() view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetLatestProtocolToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getLatestProtocolToken")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLatestProtocolToken is a free data retrieval call binding the contract method 0x5b1fcd6d.
//
// Solidity: function getLatestProtocolToken() view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetLatestProtocolToken() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestProtocolToken(&_IdentityRegistry.CallOpts)
}

// GetLatestProtocolToken is a free data retrieval call binding the contract method 0x5b1fcd6d.
//
// Solidity: function getLatestProtocolToken() view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetLatestProtocolToken() (common.Address, error) {
	return _IdentityRegistry.Contract.GetLatestProtocolToken(&_IdentityRegistry.CallOpts)
}

// GetRoleAdmin is a free data retrieval call binding the contract method 0x248a9ca3.
//
// Solidity: function getRoleAdmin(bytes32 role) view returns(bytes32)
func (_IdentityRegistry *IdentityRegistryCaller) GetRoleAdmin(opts *bind.CallOpts, role [32]byte) ([32]byte, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getRoleAdmin", role)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// GetRoleAdmin is a free data retrieval call binding the contract method 0x248a9ca3.
//
// Solidity: function getRoleAdmin(bytes32 role) view returns(bytes32)
func (_IdentityRegistry *IdentityRegistrySession) GetRoleAdmin(role [32]byte) ([32]byte, error) {
	return _IdentityRegistry.Contract.GetRoleAdmin(&_IdentityRegistry.CallOpts, role)
}

// GetRoleAdmin is a free data retrieval call binding the contract method 0x248a9ca3.
//
// Solidity: function getRoleAdmin(bytes32 role) view returns(bytes32)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetRoleAdmin(role [32]byte) ([32]byte, error) {
	return _IdentityRegistry.Contract.GetRoleAdmin(&_IdentityRegistry.CallOpts, role)
}

// GetRoleMember is a free data retrieval call binding the contract method 0x9010d07c.
//
// Solidity: function getRoleMember(bytes32 role, uint256 index) view returns(address)
func (_IdentityRegistry *IdentityRegistryCaller) GetRoleMember(opts *bind.CallOpts, role [32]byte, index *big.Int) (common.Address, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getRoleMember", role, index)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetRoleMember is a free data retrieval call binding the contract method 0x9010d07c.
//
// Solidity: function getRoleMember(bytes32 role, uint256 index) view returns(address)
func (_IdentityRegistry *IdentityRegistrySession) GetRoleMember(role [32]byte, index *big.Int) (common.Address, error) {
	return _IdentityRegistry.Contract.GetRoleMember(&_IdentityRegistry.CallOpts, role, index)
}

// GetRoleMember is a free data retrieval call binding the contract method 0x9010d07c.
//
// Solidity: function getRoleMember(bytes32 role, uint256 index) view returns(address)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetRoleMember(role [32]byte, index *big.Int) (common.Address, error) {
	return _IdentityRegistry.Contract.GetRoleMember(&_IdentityRegistry.CallOpts, role, index)
}

// GetRoleMemberCount is a free data retrieval call binding the contract method 0xca15c873.
//
// Solidity: function getRoleMemberCount(bytes32 role) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) GetRoleMemberCount(opts *bind.CallOpts, role [32]byte) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "getRoleMemberCount", role)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetRoleMemberCount is a free data retrieval call binding the contract method 0xca15c873.
//
// Solidity: function getRoleMemberCount(bytes32 role) view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) GetRoleMemberCount(role [32]byte) (*big.Int, error) {
	return _IdentityRegistry.Contract.GetRoleMemberCount(&_IdentityRegistry.CallOpts, role)
}

// GetRoleMemberCount is a free data retrieval call binding the contract method 0xca15c873.
//
// Solidity: function getRoleMemberCount(bytes32 role) view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) GetRoleMemberCount(role [32]byte) (*big.Int, error) {
	return _IdentityRegistry.Contract.GetRoleMemberCount(&_IdentityRegistry.CallOpts, role)
}

// HasRole is a free data retrieval call binding the contract method 0x91d14854.
//
// Solidity: function hasRole(bytes32 role, address account) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) HasRole(opts *bind.CallOpts, role [32]byte, account common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "hasRole", role, account)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// HasRole is a free data retrieval call binding the contract method 0x91d14854.
//
// Solidity: function hasRole(bytes32 role, address account) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) HasRole(role [32]byte, account common.Address) (bool, error) {
	return _IdentityRegistry.Contract.HasRole(&_IdentityRegistry.CallOpts, role, account)
}

// HasRole is a free data retrieval call binding the contract method 0x91d14854.
//
// Solidity: function hasRole(bytes32 role, address account) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) HasRole(role [32]byte, account common.Address) (bool, error) {
	return _IdentityRegistry.Contract.HasRole(&_IdentityRegistry.CallOpts, role, account)
}

// IsAuditor is a free data retrieval call binding the contract method 0x49b90557.
//
// Solidity: function isAuditor(address auditor) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsAuditor(opts *bind.CallOpts, auditor common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isAuditor", auditor)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsAuditor is a free data retrieval call binding the contract method 0x49b90557.
//
// Solidity: function isAuditor(address auditor) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsAuditor(auditor common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsAuditor(&_IdentityRegistry.CallOpts, auditor)
}

// IsAuditor is a free data retrieval call binding the contract method 0x49b90557.
//
// Solidity: function isAuditor(address auditor) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsAuditor(auditor common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsAuditor(&_IdentityRegistry.CallOpts, auditor)
}

// IsProtocolToken is a free data retrieval call binding the contract method 0x1549a3e1.
//
// Solidity: function isProtocolToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsProtocolToken(opts *bind.CallOpts, token common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isProtocolToken", token)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsProtocolToken is a free data retrieval call binding the contract method 0x1549a3e1.
//
// Solidity: function isProtocolToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsProtocolToken(token common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsProtocolToken(&_IdentityRegistry.CallOpts, token)
}

// IsProtocolToken is a free data retrieval call binding the contract method 0x1549a3e1.
//
// Solidity: function isProtocolToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsProtocolToken(token common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsProtocolToken(&_IdentityRegistry.CallOpts, token)
}

// IsSDPAdmin is a free data retrieval call binding the contract method 0x02e08ba4.
//
// Solidity: function isSDPAdmin(address admin) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsSDPAdmin(opts *bind.CallOpts, admin common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isSDPAdmin", admin)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsSDPAdmin is a free data retrieval call binding the contract method 0x02e08ba4.
//
// Solidity: function isSDPAdmin(address admin) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsSDPAdmin(admin common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsSDPAdmin(&_IdentityRegistry.CallOpts, admin)
}

// IsSDPAdmin is a free data retrieval call binding the contract method 0x02e08ba4.
//
// Solidity: function isSDPAdmin(address admin) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsSDPAdmin(admin common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsSDPAdmin(&_IdentityRegistry.CallOpts, admin)
}

// IsTradeableToken is a free data retrieval call binding the contract method 0x8d0dc0fa.
//
// Solidity: function isTradeableToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsTradeableToken(opts *bind.CallOpts, token common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isTradeableToken", token)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsTradeableToken is a free data retrieval call binding the contract method 0x8d0dc0fa.
//
// Solidity: function isTradeableToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsTradeableToken(token common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsTradeableToken(&_IdentityRegistry.CallOpts, token)
}

// IsTradeableToken is a free data retrieval call binding the contract method 0x8d0dc0fa.
//
// Solidity: function isTradeableToken(address token) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsTradeableToken(token common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsTradeableToken(&_IdentityRegistry.CallOpts, token)
}

// IsUnexpiredCoordinator is a free data retrieval call binding the contract method 0x56925ed2.
//
// Solidity: function isUnexpiredCoordinator(address coord) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsUnexpiredCoordinator(opts *bind.CallOpts, coord common.Address) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isUnexpiredCoordinator", coord)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsUnexpiredCoordinator is a free data retrieval call binding the contract method 0x56925ed2.
//
// Solidity: function isUnexpiredCoordinator(address coord) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsUnexpiredCoordinator(coord common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsUnexpiredCoordinator(&_IdentityRegistry.CallOpts, coord)
}

// IsUnexpiredCoordinator is a free data retrieval call binding the contract method 0x56925ed2.
//
// Solidity: function isUnexpiredCoordinator(address coord) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsUnexpiredCoordinator(coord common.Address) (bool, error) {
	return _IdentityRegistry.Contract.IsUnexpiredCoordinator(&_IdentityRegistry.CallOpts, coord)
}

// IsUnexpiredCoordinator0 is a free data retrieval call binding the contract method 0x6287f696.
//
// Solidity: function isUnexpiredCoordinator(address coord, uint256 versionId, uint256 minVersionId) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) IsUnexpiredCoordinator0(opts *bind.CallOpts, coord common.Address, versionId *big.Int, minVersionId *big.Int) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "isUnexpiredCoordinator0", coord, versionId, minVersionId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsUnexpiredCoordinator0 is a free data retrieval call binding the contract method 0x6287f696.
//
// Solidity: function isUnexpiredCoordinator(address coord, uint256 versionId, uint256 minVersionId) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) IsUnexpiredCoordinator0(coord common.Address, versionId *big.Int, minVersionId *big.Int) (bool, error) {
	return _IdentityRegistry.Contract.IsUnexpiredCoordinator0(&_IdentityRegistry.CallOpts, coord, versionId, minVersionId)
}

// IsUnexpiredCoordinator0 is a free data retrieval call binding the contract method 0x6287f696.
//
// Solidity: function isUnexpiredCoordinator(address coord, uint256 versionId, uint256 minVersionId) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) IsUnexpiredCoordinator0(coord common.Address, versionId *big.Int, minVersionId *big.Int) (bool, error) {
	return _IdentityRegistry.Contract.IsUnexpiredCoordinator0(&_IdentityRegistry.CallOpts, coord, versionId, minVersionId)
}

// ProtocolStakeAmount is a free data retrieval call binding the contract method 0xf0f13e31.
//
// Solidity: function protocolStakeAmount() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) ProtocolStakeAmount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "protocolStakeAmount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ProtocolStakeAmount is a free data retrieval call binding the contract method 0xf0f13e31.
//
// Solidity: function protocolStakeAmount() view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) ProtocolStakeAmount() (*big.Int, error) {
	return _IdentityRegistry.Contract.ProtocolStakeAmount(&_IdentityRegistry.CallOpts)
}

// ProtocolStakeAmount is a free data retrieval call binding the contract method 0xf0f13e31.
//
// Solidity: function protocolStakeAmount() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) ProtocolStakeAmount() (*big.Int, error) {
	return _IdentityRegistry.Contract.ProtocolStakeAmount(&_IdentityRegistry.CallOpts)
}

// Signatures is a free data retrieval call binding the contract method 0xc792f36d.
//
// Solidity: function signatures(address ) view returns(uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryCaller) Signatures(opts *bind.CallOpts, arg0 common.Address) (struct {
	V         uint8
	R         [32]byte
	S         [32]byte
	PublicKey []byte
}, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "signatures", arg0)

	outstruct := new(struct {
		V         uint8
		R         [32]byte
		S         [32]byte
		PublicKey []byte
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.V = *abi.ConvertType(out[0], new(uint8)).(*uint8)
	outstruct.R = *abi.ConvertType(out[1], new([32]byte)).(*[32]byte)
	outstruct.S = *abi.ConvertType(out[2], new([32]byte)).(*[32]byte)
	outstruct.PublicKey = *abi.ConvertType(out[3], new([]byte)).(*[]byte)

	return *outstruct, err

}

// Signatures is a free data retrieval call binding the contract method 0xc792f36d.
//
// Solidity: function signatures(address ) view returns(uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistrySession) Signatures(arg0 common.Address) (struct {
	V         uint8
	R         [32]byte
	S         [32]byte
	PublicKey []byte
}, error) {
	return _IdentityRegistry.Contract.Signatures(&_IdentityRegistry.CallOpts, arg0)
}

// Signatures is a free data retrieval call binding the contract method 0xc792f36d.
//
// Solidity: function signatures(address ) view returns(uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryCallerSession) Signatures(arg0 common.Address) (struct {
	V         uint8
	R         [32]byte
	S         [32]byte
	PublicKey []byte
}, error) {
	return _IdentityRegistry.Contract.Signatures(&_IdentityRegistry.CallOpts, arg0)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCaller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_IdentityRegistry *IdentityRegistrySession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _IdentityRegistry.Contract.SupportsInterface(&_IdentityRegistry.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_IdentityRegistry *IdentityRegistryCallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _IdentityRegistry.Contract.SupportsInterface(&_IdentityRegistry.CallOpts, interfaceId)
}

// UnlockInterval is a free data retrieval call binding the contract method 0x343054cd.
//
// Solidity: function unlockInterval() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCaller) UnlockInterval(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "unlockInterval")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// UnlockInterval is a free data retrieval call binding the contract method 0x343054cd.
//
// Solidity: function unlockInterval() view returns(uint256)
func (_IdentityRegistry *IdentityRegistrySession) UnlockInterval() (*big.Int, error) {
	return _IdentityRegistry.Contract.UnlockInterval(&_IdentityRegistry.CallOpts)
}

// UnlockInterval is a free data retrieval call binding the contract method 0x343054cd.
//
// Solidity: function unlockInterval() view returns(uint256)
func (_IdentityRegistry *IdentityRegistryCallerSession) UnlockInterval() (*big.Int, error) {
	return _IdentityRegistry.Contract.UnlockInterval(&_IdentityRegistry.CallOpts)
}

// VersionInfo is a free data retrieval call binding the contract method 0x58fdd506.
//
// Solidity: function versionInfo(address ) view returns(uint256 id, uint256 expirationDate)
func (_IdentityRegistry *IdentityRegistryCaller) VersionInfo(opts *bind.CallOpts, arg0 common.Address) (struct {
	Id             *big.Int
	ExpirationDate *big.Int
}, error) {
	var out []interface{}
	err := _IdentityRegistry.contract.Call(opts, &out, "versionInfo", arg0)

	outstruct := new(struct {
		Id             *big.Int
		ExpirationDate *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.Id = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.ExpirationDate = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// VersionInfo is a free data retrieval call binding the contract method 0x58fdd506.
//
// Solidity: function versionInfo(address ) view returns(uint256 id, uint256 expirationDate)
func (_IdentityRegistry *IdentityRegistrySession) VersionInfo(arg0 common.Address) (struct {
	Id             *big.Int
	ExpirationDate *big.Int
}, error) {
	return _IdentityRegistry.Contract.VersionInfo(&_IdentityRegistry.CallOpts, arg0)
}

// VersionInfo is a free data retrieval call binding the contract method 0x58fdd506.
//
// Solidity: function versionInfo(address ) view returns(uint256 id, uint256 expirationDate)
func (_IdentityRegistry *IdentityRegistryCallerSession) VersionInfo(arg0 common.Address) (struct {
	Id             *big.Int
	ExpirationDate *big.Int
}, error) {
	return _IdentityRegistry.Contract.VersionInfo(&_IdentityRegistry.CallOpts, arg0)
}

// ApproveSignature is a paid mutator transaction binding the contract method 0xf59fbea9.
//
// Solidity: function approveSignature(uint8 v, bytes32 r, bytes32 s, bytes publicKey) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) ApproveSignature(opts *bind.TransactOpts, v uint8, r [32]byte, s [32]byte, publicKey []byte) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "approveSignature", v, r, s, publicKey)
}

// ApproveSignature is a paid mutator transaction binding the contract method 0xf59fbea9.
//
// Solidity: function approveSignature(uint8 v, bytes32 r, bytes32 s, bytes publicKey) returns()
func (_IdentityRegistry *IdentityRegistrySession) ApproveSignature(v uint8, r [32]byte, s [32]byte, publicKey []byte) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.ApproveSignature(&_IdentityRegistry.TransactOpts, v, r, s, publicKey)
}

// ApproveSignature is a paid mutator transaction binding the contract method 0xf59fbea9.
//
// Solidity: function approveSignature(uint8 v, bytes32 r, bytes32 s, bytes publicKey) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) ApproveSignature(v uint8, r [32]byte, s [32]byte, publicKey []byte) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.ApproveSignature(&_IdentityRegistry.TransactOpts, v, r, s, publicKey)
}

// GrantRole is a paid mutator transaction binding the contract method 0x2f2ff15d.
//
// Solidity: function grantRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) GrantRole(opts *bind.TransactOpts, role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "grantRole", role, account)
}

// GrantRole is a paid mutator transaction binding the contract method 0x2f2ff15d.
//
// Solidity: function grantRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistrySession) GrantRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.GrantRole(&_IdentityRegistry.TransactOpts, role, account)
}

// GrantRole is a paid mutator transaction binding the contract method 0x2f2ff15d.
//
// Solidity: function grantRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) GrantRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.GrantRole(&_IdentityRegistry.TransactOpts, role, account)
}

// GrantSDPAdmin is a paid mutator transaction binding the contract method 0x00202457.
//
// Solidity: function grantSDPAdmin(address admin) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) GrantSDPAdmin(opts *bind.TransactOpts, admin common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "grantSDPAdmin", admin)
}

// GrantSDPAdmin is a paid mutator transaction binding the contract method 0x00202457.
//
// Solidity: function grantSDPAdmin(address admin) returns()
func (_IdentityRegistry *IdentityRegistrySession) GrantSDPAdmin(admin common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.GrantSDPAdmin(&_IdentityRegistry.TransactOpts, admin)
}

// GrantSDPAdmin is a paid mutator transaction binding the contract method 0x00202457.
//
// Solidity: function grantSDPAdmin(address admin) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) GrantSDPAdmin(admin common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.GrantSDPAdmin(&_IdentityRegistry.TransactOpts, admin)
}

// InitializeRole is a paid mutator transaction binding the contract method 0x4f794969.
//
// Solidity: function initializeRole(bytes32 role, address initialAddress) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) InitializeRole(opts *bind.TransactOpts, role [32]byte, initialAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "initializeRole", role, initialAddress)
}

// InitializeRole is a paid mutator transaction binding the contract method 0x4f794969.
//
// Solidity: function initializeRole(bytes32 role, address initialAddress) returns()
func (_IdentityRegistry *IdentityRegistrySession) InitializeRole(role [32]byte, initialAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.InitializeRole(&_IdentityRegistry.TransactOpts, role, initialAddress)
}

// InitializeRole is a paid mutator transaction binding the contract method 0x4f794969.
//
// Solidity: function initializeRole(bytes32 role, address initialAddress) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) InitializeRole(role [32]byte, initialAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.InitializeRole(&_IdentityRegistry.TransactOpts, role, initialAddress)
}

// RenounceRole is a paid mutator transaction binding the contract method 0x36568abe.
//
// Solidity: function renounceRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) RenounceRole(opts *bind.TransactOpts, role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "renounceRole", role, account)
}

// RenounceRole is a paid mutator transaction binding the contract method 0x36568abe.
//
// Solidity: function renounceRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistrySession) RenounceRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RenounceRole(&_IdentityRegistry.TransactOpts, role, account)
}

// RenounceRole is a paid mutator transaction binding the contract method 0x36568abe.
//
// Solidity: function renounceRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) RenounceRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RenounceRole(&_IdentityRegistry.TransactOpts, role, account)
}

// RevokeRole is a paid mutator transaction binding the contract method 0xd547741f.
//
// Solidity: function revokeRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) RevokeRole(opts *bind.TransactOpts, role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "revokeRole", role, account)
}

// RevokeRole is a paid mutator transaction binding the contract method 0xd547741f.
//
// Solidity: function revokeRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistrySession) RevokeRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RevokeRole(&_IdentityRegistry.TransactOpts, role, account)
}

// RevokeRole is a paid mutator transaction binding the contract method 0xd547741f.
//
// Solidity: function revokeRole(bytes32 role, address account) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) RevokeRole(role [32]byte, account common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RevokeRole(&_IdentityRegistry.TransactOpts, role, account)
}

// RevokeSignature is a paid mutator transaction binding the contract method 0x233c8cad.
//
// Solidity: function revokeSignature() returns()
func (_IdentityRegistry *IdentityRegistryTransactor) RevokeSignature(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "revokeSignature")
}

// RevokeSignature is a paid mutator transaction binding the contract method 0x233c8cad.
//
// Solidity: function revokeSignature() returns()
func (_IdentityRegistry *IdentityRegistrySession) RevokeSignature() (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RevokeSignature(&_IdentityRegistry.TransactOpts)
}

// RevokeSignature is a paid mutator transaction binding the contract method 0x233c8cad.
//
// Solidity: function revokeSignature() returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) RevokeSignature() (*types.Transaction, error) {
	return _IdentityRegistry.Contract.RevokeSignature(&_IdentityRegistry.TransactOpts)
}

// SetFeeRecipient is a paid mutator transaction binding the contract method 0xe74b981b.
//
// Solidity: function setFeeRecipient(address _feeRecipient) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) SetFeeRecipient(opts *bind.TransactOpts, _feeRecipient common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "setFeeRecipient", _feeRecipient)
}

// SetFeeRecipient is a paid mutator transaction binding the contract method 0xe74b981b.
//
// Solidity: function setFeeRecipient(address _feeRecipient) returns()
func (_IdentityRegistry *IdentityRegistrySession) SetFeeRecipient(_feeRecipient common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.SetFeeRecipient(&_IdentityRegistry.TransactOpts, _feeRecipient)
}

// SetFeeRecipient is a paid mutator transaction binding the contract method 0xe74b981b.
//
// Solidity: function setFeeRecipient(address _feeRecipient) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) SetFeeRecipient(_feeRecipient common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.SetFeeRecipient(&_IdentityRegistry.TransactOpts, _feeRecipient)
}

// SetStakeAmount is a paid mutator transaction binding the contract method 0x43808c50.
//
// Solidity: function setStakeAmount(uint256 _protocolStakeAmount) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) SetStakeAmount(opts *bind.TransactOpts, _protocolStakeAmount *big.Int) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "setStakeAmount", _protocolStakeAmount)
}

// SetStakeAmount is a paid mutator transaction binding the contract method 0x43808c50.
//
// Solidity: function setStakeAmount(uint256 _protocolStakeAmount) returns()
func (_IdentityRegistry *IdentityRegistrySession) SetStakeAmount(_protocolStakeAmount *big.Int) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.SetStakeAmount(&_IdentityRegistry.TransactOpts, _protocolStakeAmount)
}

// SetStakeAmount is a paid mutator transaction binding the contract method 0x43808c50.
//
// Solidity: function setStakeAmount(uint256 _protocolStakeAmount) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) SetStakeAmount(_protocolStakeAmount *big.Int) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.SetStakeAmount(&_IdentityRegistry.TransactOpts, _protocolStakeAmount)
}

// UpdateRole is a paid mutator transaction binding the contract method 0x4e603040.
//
// Solidity: function updateRole(bytes32 role, address newAddress) returns()
func (_IdentityRegistry *IdentityRegistryTransactor) UpdateRole(opts *bind.TransactOpts, role [32]byte, newAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.contract.Transact(opts, "updateRole", role, newAddress)
}

// UpdateRole is a paid mutator transaction binding the contract method 0x4e603040.
//
// Solidity: function updateRole(bytes32 role, address newAddress) returns()
func (_IdentityRegistry *IdentityRegistrySession) UpdateRole(role [32]byte, newAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.UpdateRole(&_IdentityRegistry.TransactOpts, role, newAddress)
}

// UpdateRole is a paid mutator transaction binding the contract method 0x4e603040.
//
// Solidity: function updateRole(bytes32 role, address newAddress) returns()
func (_IdentityRegistry *IdentityRegistryTransactorSession) UpdateRole(role [32]byte, newAddress common.Address) (*types.Transaction, error) {
	return _IdentityRegistry.Contract.UpdateRole(&_IdentityRegistry.TransactOpts, role, newAddress)
}

// IdentityRegistryKeyApprovedIterator is returned from FilterKeyApproved and is used to iterate over the raw logs and unpacked data for KeyApproved events raised by the IdentityRegistry contract.
type IdentityRegistryKeyApprovedIterator struct {
	Event *IdentityRegistryKeyApproved // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IdentityRegistryKeyApprovedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IdentityRegistryKeyApproved)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IdentityRegistryKeyApproved)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IdentityRegistryKeyApprovedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IdentityRegistryKeyApprovedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IdentityRegistryKeyApproved represents a KeyApproved event raised by the IdentityRegistry contract.
type IdentityRegistryKeyApproved struct {
	Trader    common.Address
	V         uint8
	R         [32]byte
	S         [32]byte
	PublicKey []byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterKeyApproved is a free log retrieval operation binding the contract event 0x07c100f1c988e70a9b8c8fb95068b42285d658e85ebad57b00ccd924f39ecf14.
//
// Solidity: event KeyApproved(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) FilterKeyApproved(opts *bind.FilterOpts, trader []common.Address) (*IdentityRegistryKeyApprovedIterator, error) {

	var traderRule []interface{}
	for _, traderItem := range trader {
		traderRule = append(traderRule, traderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.FilterLogs(opts, "KeyApproved", traderRule)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryKeyApprovedIterator{contract: _IdentityRegistry.contract, event: "KeyApproved", logs: logs, sub: sub}, nil
}

// WatchKeyApproved is a free log subscription operation binding the contract event 0x07c100f1c988e70a9b8c8fb95068b42285d658e85ebad57b00ccd924f39ecf14.
//
// Solidity: event KeyApproved(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) WatchKeyApproved(opts *bind.WatchOpts, sink chan<- *IdentityRegistryKeyApproved, trader []common.Address) (event.Subscription, error) {

	var traderRule []interface{}
	for _, traderItem := range trader {
		traderRule = append(traderRule, traderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.WatchLogs(opts, "KeyApproved", traderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IdentityRegistryKeyApproved)
				if err := _IdentityRegistry.contract.UnpackLog(event, "KeyApproved", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseKeyApproved is a log parse operation binding the contract event 0x07c100f1c988e70a9b8c8fb95068b42285d658e85ebad57b00ccd924f39ecf14.
//
// Solidity: event KeyApproved(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) ParseKeyApproved(log types.Log) (*IdentityRegistryKeyApproved, error) {
	event := new(IdentityRegistryKeyApproved)
	if err := _IdentityRegistry.contract.UnpackLog(event, "KeyApproved", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IdentityRegistryKeyRevokedIterator is returned from FilterKeyRevoked and is used to iterate over the raw logs and unpacked data for KeyRevoked events raised by the IdentityRegistry contract.
type IdentityRegistryKeyRevokedIterator struct {
	Event *IdentityRegistryKeyRevoked // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IdentityRegistryKeyRevokedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IdentityRegistryKeyRevoked)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IdentityRegistryKeyRevoked)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IdentityRegistryKeyRevokedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IdentityRegistryKeyRevokedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IdentityRegistryKeyRevoked represents a KeyRevoked event raised by the IdentityRegistry contract.
type IdentityRegistryKeyRevoked struct {
	Trader    common.Address
	V         uint8
	R         [32]byte
	S         [32]byte
	PublicKey []byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterKeyRevoked is a free log retrieval operation binding the contract event 0xac0d04263a9254e9d9191dafe8285040685ea7acd489baa167894bfb33e86836.
//
// Solidity: event KeyRevoked(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) FilterKeyRevoked(opts *bind.FilterOpts, trader []common.Address) (*IdentityRegistryKeyRevokedIterator, error) {

	var traderRule []interface{}
	for _, traderItem := range trader {
		traderRule = append(traderRule, traderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.FilterLogs(opts, "KeyRevoked", traderRule)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryKeyRevokedIterator{contract: _IdentityRegistry.contract, event: "KeyRevoked", logs: logs, sub: sub}, nil
}

// WatchKeyRevoked is a free log subscription operation binding the contract event 0xac0d04263a9254e9d9191dafe8285040685ea7acd489baa167894bfb33e86836.
//
// Solidity: event KeyRevoked(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) WatchKeyRevoked(opts *bind.WatchOpts, sink chan<- *IdentityRegistryKeyRevoked, trader []common.Address) (event.Subscription, error) {

	var traderRule []interface{}
	for _, traderItem := range trader {
		traderRule = append(traderRule, traderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.WatchLogs(opts, "KeyRevoked", traderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IdentityRegistryKeyRevoked)
				if err := _IdentityRegistry.contract.UnpackLog(event, "KeyRevoked", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseKeyRevoked is a log parse operation binding the contract event 0xac0d04263a9254e9d9191dafe8285040685ea7acd489baa167894bfb33e86836.
//
// Solidity: event KeyRevoked(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey)
func (_IdentityRegistry *IdentityRegistryFilterer) ParseKeyRevoked(log types.Log) (*IdentityRegistryKeyRevoked, error) {
	event := new(IdentityRegistryKeyRevoked)
	if err := _IdentityRegistry.contract.UnpackLog(event, "KeyRevoked", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IdentityRegistryRoleAdminChangedIterator is returned from FilterRoleAdminChanged and is used to iterate over the raw logs and unpacked data for RoleAdminChanged events raised by the IdentityRegistry contract.
type IdentityRegistryRoleAdminChangedIterator struct {
	Event *IdentityRegistryRoleAdminChanged // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IdentityRegistryRoleAdminChangedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IdentityRegistryRoleAdminChanged)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IdentityRegistryRoleAdminChanged)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IdentityRegistryRoleAdminChangedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IdentityRegistryRoleAdminChangedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IdentityRegistryRoleAdminChanged represents a RoleAdminChanged event raised by the IdentityRegistry contract.
type IdentityRegistryRoleAdminChanged struct {
	Role              [32]byte
	PreviousAdminRole [32]byte
	NewAdminRole      [32]byte
	Raw               types.Log // Blockchain specific contextual infos
}

// FilterRoleAdminChanged is a free log retrieval operation binding the contract event 0xbd79b86ffe0ab8e8776151514217cd7cacd52c909f66475c3af44e129f0b00ff.
//
// Solidity: event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole)
func (_IdentityRegistry *IdentityRegistryFilterer) FilterRoleAdminChanged(opts *bind.FilterOpts, role [][32]byte, previousAdminRole [][32]byte, newAdminRole [][32]byte) (*IdentityRegistryRoleAdminChangedIterator, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var previousAdminRoleRule []interface{}
	for _, previousAdminRoleItem := range previousAdminRole {
		previousAdminRoleRule = append(previousAdminRoleRule, previousAdminRoleItem)
	}
	var newAdminRoleRule []interface{}
	for _, newAdminRoleItem := range newAdminRole {
		newAdminRoleRule = append(newAdminRoleRule, newAdminRoleItem)
	}

	logs, sub, err := _IdentityRegistry.contract.FilterLogs(opts, "RoleAdminChanged", roleRule, previousAdminRoleRule, newAdminRoleRule)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryRoleAdminChangedIterator{contract: _IdentityRegistry.contract, event: "RoleAdminChanged", logs: logs, sub: sub}, nil
}

// WatchRoleAdminChanged is a free log subscription operation binding the contract event 0xbd79b86ffe0ab8e8776151514217cd7cacd52c909f66475c3af44e129f0b00ff.
//
// Solidity: event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole)
func (_IdentityRegistry *IdentityRegistryFilterer) WatchRoleAdminChanged(opts *bind.WatchOpts, sink chan<- *IdentityRegistryRoleAdminChanged, role [][32]byte, previousAdminRole [][32]byte, newAdminRole [][32]byte) (event.Subscription, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var previousAdminRoleRule []interface{}
	for _, previousAdminRoleItem := range previousAdminRole {
		previousAdminRoleRule = append(previousAdminRoleRule, previousAdminRoleItem)
	}
	var newAdminRoleRule []interface{}
	for _, newAdminRoleItem := range newAdminRole {
		newAdminRoleRule = append(newAdminRoleRule, newAdminRoleItem)
	}

	logs, sub, err := _IdentityRegistry.contract.WatchLogs(opts, "RoleAdminChanged", roleRule, previousAdminRoleRule, newAdminRoleRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IdentityRegistryRoleAdminChanged)
				if err := _IdentityRegistry.contract.UnpackLog(event, "RoleAdminChanged", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRoleAdminChanged is a log parse operation binding the contract event 0xbd79b86ffe0ab8e8776151514217cd7cacd52c909f66475c3af44e129f0b00ff.
//
// Solidity: event RoleAdminChanged(bytes32 indexed role, bytes32 indexed previousAdminRole, bytes32 indexed newAdminRole)
func (_IdentityRegistry *IdentityRegistryFilterer) ParseRoleAdminChanged(log types.Log) (*IdentityRegistryRoleAdminChanged, error) {
	event := new(IdentityRegistryRoleAdminChanged)
	if err := _IdentityRegistry.contract.UnpackLog(event, "RoleAdminChanged", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IdentityRegistryRoleGrantedIterator is returned from FilterRoleGranted and is used to iterate over the raw logs and unpacked data for RoleGranted events raised by the IdentityRegistry contract.
type IdentityRegistryRoleGrantedIterator struct {
	Event *IdentityRegistryRoleGranted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IdentityRegistryRoleGrantedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IdentityRegistryRoleGranted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IdentityRegistryRoleGranted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IdentityRegistryRoleGrantedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IdentityRegistryRoleGrantedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IdentityRegistryRoleGranted represents a RoleGranted event raised by the IdentityRegistry contract.
type IdentityRegistryRoleGranted struct {
	Role    [32]byte
	Account common.Address
	Sender  common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterRoleGranted is a free log retrieval operation binding the contract event 0x2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d.
//
// Solidity: event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) FilterRoleGranted(opts *bind.FilterOpts, role [][32]byte, account []common.Address, sender []common.Address) (*IdentityRegistryRoleGrantedIterator, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}
	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.FilterLogs(opts, "RoleGranted", roleRule, accountRule, senderRule)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryRoleGrantedIterator{contract: _IdentityRegistry.contract, event: "RoleGranted", logs: logs, sub: sub}, nil
}

// WatchRoleGranted is a free log subscription operation binding the contract event 0x2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d.
//
// Solidity: event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) WatchRoleGranted(opts *bind.WatchOpts, sink chan<- *IdentityRegistryRoleGranted, role [][32]byte, account []common.Address, sender []common.Address) (event.Subscription, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}
	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.WatchLogs(opts, "RoleGranted", roleRule, accountRule, senderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IdentityRegistryRoleGranted)
				if err := _IdentityRegistry.contract.UnpackLog(event, "RoleGranted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRoleGranted is a log parse operation binding the contract event 0x2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d.
//
// Solidity: event RoleGranted(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) ParseRoleGranted(log types.Log) (*IdentityRegistryRoleGranted, error) {
	event := new(IdentityRegistryRoleGranted)
	if err := _IdentityRegistry.contract.UnpackLog(event, "RoleGranted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IdentityRegistryRoleRevokedIterator is returned from FilterRoleRevoked and is used to iterate over the raw logs and unpacked data for RoleRevoked events raised by the IdentityRegistry contract.
type IdentityRegistryRoleRevokedIterator struct {
	Event *IdentityRegistryRoleRevoked // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IdentityRegistryRoleRevokedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IdentityRegistryRoleRevoked)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IdentityRegistryRoleRevoked)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IdentityRegistryRoleRevokedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IdentityRegistryRoleRevokedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IdentityRegistryRoleRevoked represents a RoleRevoked event raised by the IdentityRegistry contract.
type IdentityRegistryRoleRevoked struct {
	Role    [32]byte
	Account common.Address
	Sender  common.Address
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterRoleRevoked is a free log retrieval operation binding the contract event 0xf6391f5c32d9c69d2a47ea670b442974b53935d1edc7fd64eb21e047a839171b.
//
// Solidity: event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) FilterRoleRevoked(opts *bind.FilterOpts, role [][32]byte, account []common.Address, sender []common.Address) (*IdentityRegistryRoleRevokedIterator, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}
	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.FilterLogs(opts, "RoleRevoked", roleRule, accountRule, senderRule)
	if err != nil {
		return nil, err
	}
	return &IdentityRegistryRoleRevokedIterator{contract: _IdentityRegistry.contract, event: "RoleRevoked", logs: logs, sub: sub}, nil
}

// WatchRoleRevoked is a free log subscription operation binding the contract event 0xf6391f5c32d9c69d2a47ea670b442974b53935d1edc7fd64eb21e047a839171b.
//
// Solidity: event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) WatchRoleRevoked(opts *bind.WatchOpts, sink chan<- *IdentityRegistryRoleRevoked, role [][32]byte, account []common.Address, sender []common.Address) (event.Subscription, error) {

	var roleRule []interface{}
	for _, roleItem := range role {
		roleRule = append(roleRule, roleItem)
	}
	var accountRule []interface{}
	for _, accountItem := range account {
		accountRule = append(accountRule, accountItem)
	}
	var senderRule []interface{}
	for _, senderItem := range sender {
		senderRule = append(senderRule, senderItem)
	}

	logs, sub, err := _IdentityRegistry.contract.WatchLogs(opts, "RoleRevoked", roleRule, accountRule, senderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IdentityRegistryRoleRevoked)
				if err := _IdentityRegistry.contract.UnpackLog(event, "RoleRevoked", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRoleRevoked is a log parse operation binding the contract event 0xf6391f5c32d9c69d2a47ea670b442974b53935d1edc7fd64eb21e047a839171b.
//
// Solidity: event RoleRevoked(bytes32 indexed role, address indexed account, address indexed sender)
func (_IdentityRegistry *IdentityRegistryFilterer) ParseRoleRevoked(log types.Log) (*IdentityRegistryRoleRevoked, error) {
	event := new(IdentityRegistryRoleRevoked)
	if err := _IdentityRegistry.contract.UnpackLog(event, "RoleRevoked", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
