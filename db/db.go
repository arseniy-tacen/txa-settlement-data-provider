// Copyright © 2023 TXA PTE. LTD.

package db

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	"sdp/config"

	l "sdp/logger"
	"sdp/model"
	"sdp/retry"

	"github.com/DATA-DOG/go-txdb"
	"github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	initializeOnce sync.Once
	log            = l.Default
)

func NewDb(connectionString string) (*gorm.DB, error) {
	if err := initializeDatabase(); err != nil {
		panic(err)
	}

	var db *gorm.DB
	connectionAttempt := func(_ uint) error {
		var err error
		db, err = gorm.Open(postgres.Open(connectionString), &gorm.Config{
			PrepareStmt:            true,
			SkipDefaultTransaction: true,
			Logger:                 logger.Default.LogMode(logger.Silent),
		})
		if err != nil {
			return err
		}

		return err
	}
	if err := retry.DoWithLimitAndFixedBackoff(connectionAttempt, 15, time.Second*1); err != nil {
		return nil, err
	}

	AutoMigrate(db)
	CreateTriggers(db)

	return db, nil
}

func CreateTriggers(db *gorm.DB) {
	createTradeNotification(db)
	createOrderNotification(db)
	createEventNotification(db)
	createTradesTrigger(db)
	createOrdersTrigger(db)
	createEventsTrigger(db)
}

func AutoMigrate(db *gorm.DB) error {
	if err := db.AutoMigrate(
		&model.Obligation{},
		&model.Trade{},
		&model.RejectedTrade{},
		&model.LastProcessedTrade{},
		&model.OrdersCanceledForSettlement{},
		&model.Event{},
	); err != nil {
		return err
	}
	db.Create(&model.LastProcessedTrade{SdpSequenceNumber: 0})
	return nil
}

func NewTestDb(connectionString string) (*gorm.DB, error) {
	initializeOnce.Do(func() {
		txdb.Register("txdb", "postgres", connectionString)
	})

	conn, err := sql.Open("txdb", "testDatabase")
	if err != nil {
		return nil, err
	}

	pg := postgres.New(postgres.Config{Conn: conn})

	db, err := gorm.Open(pg, &gorm.Config{Logger: logger.Default.LogMode(logger.Silent)})
	if err != nil {
		return nil, err
	}

	AutoMigrate(db)

	return db, nil
}

func initializeDatabase() error {
	connectionString := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s",
		config.GetPgUser(),
		config.GetPgPassword(),
		config.GetPgHost(),
		config.GetPgPort(),
		"postgres")

	var conn *gorm.DB
	connectionAttempt := func(uint) error {
		var err error
		conn, err = gorm.Open(postgres.Open(connectionString), &gorm.Config{Logger: logger.Default})
		return err
	}
	if err := retry.DoWithLimitAndFixedBackoff(connectionAttempt, 15, time.Second*1); err != nil {
		panic(err)
	}

	stmt := fmt.Sprintf("SELECT * FROM pg_database WHERE datname = '%s';", config.GetPgDb())
	rs := conn.Raw(stmt)
	if rs.Error != nil {
		return rs.Error
	}

	rec := make(map[string]interface{})
	if rs.Find(rec); len(rec) == 0 {
		stmt := fmt.Sprintf("CREATE DATABASE %s;", config.GetPgDb())
		if rs := conn.Exec(stmt); rs.Error != nil {
			return rs.Error
		}
	}

	sql, err := conn.DB()
	if err != nil {
		return err
	}
	if err := sql.Close(); err != nil {
		return err
	}

	return nil
}

// NewListener returns an interface for listening to DB notifications
func NewListener() *pq.Listener {
	return pq.NewListener(config.GetPgURL()+"?sslmode=disable",
		10*time.Second,
		time.Minute,
		func(_ pq.ListenerEventType, err error) {
			if err != nil {
				log.Error(err.Error())
			}
		})
}

func createTradesTrigger(db *gorm.DB) error {
	return db.Exec(`CREATE TRIGGER trade_insert
    AFTER INSERT ON trades
    FOR EACH ROW
    EXECUTE PROCEDURE notify_trade()
    `).Error
}

func createOrdersTrigger(db *gorm.DB) error {
	return db.Exec(`CREATE TRIGGER order_insert
    AFTER INSERT ON orders_canceled_for_settlements
    FOR EACH ROW
    EXECUTE PROCEDURE notify_order()
    `).Error
}

func createEventsTrigger(db *gorm.DB) error {
	return db.Exec(`CREATE TRIGGER event_insert
    AFTER INSERT ON events
    FOR EACH ROW
    EXECUTE PROCEDURE notify_event()
    `).Error
}

func createTradeNotification(db *gorm.DB) error {
	return db.Exec(
		`CREATE OR REPLACE FUNCTION notify_trade()
        RETURNS TRIGGER AS $$
        BEGIN
            PERFORM pg_notify('trade_channel', NEW.sdp_sequence_number::text);
            RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
    `).Error
}

func createOrderNotification(db *gorm.DB) error {
	return db.Exec(
		`CREATE OR REPLACE FUNCTION notify_order()
        RETURNS TRIGGER AS $$
        BEGIN
            PERFORM pg_notify('order_channel', NEW.sdp_sequence_number::text);
            RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
    `).Error
}

func createEventNotification(db *gorm.DB) error {
	return db.Exec(
		`CREATE OR REPLACE FUNCTION notify_event()
        RETURNS TRIGGER AS $$
        BEGIN
            PERFORM pg_notify('event_channel', NEW.tx_hash::text);
            RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
    `).Error
}
