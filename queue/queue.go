// Copyright © 2023 TXA PTE. LTD.

package queue

import (
	"context"
	"fmt"
	"time"

	"sdp/logger"
	"sdp/retry"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type Queue struct {
	client *redis.Client
	logger *logrus.Logger
}

func NewQueue(connectionString, password string) (*Queue, error) {
	logger := logger.Default

	rq := &Queue{
		logger: logger,
	}

	var client *redis.Client
	connectionAttempt := func(_ uint) error {
		var err error
		client = redis.NewClient(&redis.Options{
			Addr:     connectionString,
			Password: password,
			DB:       0,
		})
		if _, err = client.Ping(context.Background()).Result(); err != nil {
			logger.Warn("failed to connect to redis, retrying")
		}
		return err
	}
	if err := retry.DoWithLimitAndFixedBackoff(connectionAttempt, 15, time.Second*1); err != nil {
		return nil, fmt.Errorf("exceeded maximum number of retries")
	}

	rq.client = client

	return rq, nil
}

func (q *Queue) Subscribe(ctx context.Context, key string) *redis.PubSub {
	return q.client.Subscribe(ctx, key)
}

func (q *Queue) Publish(ctx context.Context, key string, value any) error {
	for i := 0; i < 15; i++ {
		if err := q.client.Publish(ctx, key, value).Err(); err == nil {
			return nil
		}
		time.Sleep(time.Millisecond * 250)
	}
	return fmt.Errorf("failed to push value to Redis queue after %d attempts", 15)
}

func (q *Queue) Get(ctx context.Context, key string) (string, error) {
	return q.client.Get(ctx, key).Result()
}

func (q *Queue) Set(ctx context.Context, key string, value any) error {
	return q.client.Set(ctx, key, value, time.Minute*60).Err()
}

func (q *Queue) Exists(ctx context.Context, key string) (int64, error) {
	return q.client.Exists(ctx, key).Result()
}

func (q *Queue) Ping() error {
	if err := q.client.Ping(context.Background()).Err(); err != nil {
		return err
	}
	return nil
}
